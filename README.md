# Templates for Use in Doxa

These templates are written in the Liturgical Markup Language and were converted from [AGES ALWB Templates](https://github.com/AGES-Initiatives/ages-alwb-templates/tree/master/net.ages.liturgical.workbench.templates).  The ALWB templates were created by Fr. Seraphim Dedes and are currently used to generate the [Digital Chant Stand](https://goa.goarch.org) used by the Greek Orthodox Archdiocese of America.  The converted templates are not ready for production use, but can be used for beta testing.
